<?php 
use COVID_19\COVID_19;

require_once realpath("./vendor/autoload.php");

$COVID_19 = new COVID_19();
$data = $COVID_19->getDataByCountry('lebanon');
$data = json_decode($data, true);
$data = $data['response'];
?>

<html>
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<head>
<body>

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Country</th>
      <th scope="col">Total Cases</th>
      <th scope="col">New Cases</th>
      <th scope="col">Total Deaths</th>
      <th scope="col">New Deaths</th>
      <th scope="col">Total Recovered</th>
      <th scope="col">Active Cases</th>
      <th scope="col">Day</th>
    </tr>
  </thead>
  <tbody>
    <?php 
    foreach ($data as $value) {
    ?>
    <tr>
      <th scope="row"><?php echo $value['country']  ?></th>
      <td><?php echo $value['cases']['total']  ?></td>
      <td><?php echo $value['cases']['new']  ?></td>
      <td><?php echo $value['deaths']['total']  ?></td>
      <td><?php echo $value['deaths']['new']  ?></td>
      <td><?php echo $value['cases']['recovered']  ?></td>
      <td><?php echo $value['cases']['active']  ?></td>
      <td><?php echo $value['day']  ?></td>
   </tr>
    <?php } ?>
  </tbody>
</table>

<body>
<html>
