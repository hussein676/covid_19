<?php
namespace COVID_19;

class COVID_19 {
    
   public $key="26eb1599b9msh6dd73ec467dede0p11c63bjsn6838fcb2e324";
    function __construct(){
   
   }
  function getDataByCountry($country){
  
    $curl = curl_init();
    
    curl_setopt_array($curl, [
      CURLOPT_URL => "https://covid-193.p.rapidapi.com/statistics?country=".$country,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => [
        "x-rapidapi-host: covid-193.p.rapidapi.com",
        "x-rapidapi-key: ".$this->key
      ],
    ]);
    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      
      return $response;
    }
  }

}
