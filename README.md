# COVID-19 

## Description 
> This project gets statistics of COVID-19 live cases in Lebanon. 

## Installation
>"git config --global user.email "husein.m.hammoud@gmail.com"

>if you have composer installed globally run this command:

>cd covid-19

>composer dump-autoload -o
>if you don't have :
>cd covid-19
>php composer.phar install | php composer.phar dump-autoload -o

## Access 
> To access the data :

>In your index.php file add the following :
```php
		use COVID_19\COVID_19;
		require_once realpath("./vendor/autoload.php");
		$COVID_19 = new COVID_19();
		$data = $COVID_19->getDataByCountry('{country}');
		$data = json_decode($data, true);
		var_dump($data);
```
> for lebanon {country}=lebanon;
